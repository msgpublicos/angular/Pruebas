import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ArrayService {
  sApi = 'https://localhost:7246/Array';

  constructor(private http: HttpClient) {}

  getConArray(arr: string[]): Observable<any> {
    let arreglo: string = '';
    let sSep: string = '?arr=';
    console.log('arr=', arr);
    arr.forEach((element) => {
      console.log(element);
      arreglo += sSep + element;
      sSep = '&arr=';
    });
    console.log(this.sApi + arreglo);
    return this.http.get<any>(this.sApi + arreglo);
  }
}
