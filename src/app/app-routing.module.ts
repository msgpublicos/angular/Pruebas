import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GraficosComponent } from './components/graficos/graficos.component';
import { PasswordComponent } from './components/password/password.component';
import { Base64Component } from './components/base64/base64.component';
import { EncriptaComponent } from './components/encripta/encripta.component';

const routes: Routes = [
  { path: 'graficos', component: GraficosComponent },
  { path: 'password', component: PasswordComponent },
  { path: 'base64', component: Base64Component },
  { path: 'encripta', component: EncriptaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
