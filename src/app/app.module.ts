import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArrayComponent } from './components/array/array.component';
import { ExcelComponent } from './components/excel/excel.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PasswordStrengthComponent } from './components/password-strength/password-strength.component';
import { GraficosComponent } from './components/graficos/graficos.component';
import { PasswordComponent } from './components/password/password.component';
import { Base64Component } from './components/base64/base64.component';
import { EncriptaComponent } from './components/encripta/encripta.component';

@NgModule({
  declarations: [
    AppComponent,
    ArrayComponent,
    ExcelComponent,
    PasswordStrengthComponent,
    GraficosComponent,
    PasswordComponent,
    Base64Component,
    EncriptaComponent,
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
