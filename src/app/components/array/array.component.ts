import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ArrayService } from 'src/app/Service/array.service';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css'],
})
export class ArrayComponent implements OnInit {
  constructor(private http: HttpClient, private _Array: ArrayService) {}

  ngOnInit(): void {}

  onArray() {
    let arr: string[] = [];
    arr.push('1');
    arr.push('3');
    arr.push('texto largo');

    this._Array.getConArray(arr).subscribe((data) => {});
  }
}
