import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base64',
  templateUrl: './base64.component.html',
  styleUrls: ['./base64.component.css']
})
export class Base64Component implements OnInit {
  imagenBase64: string = '';


  constructor() { }

  ngOnInit(): void {
  }

  onFileSelected(event: any){
    const file: File = event.target.files[0];
    const reader = new FileReader();

    reader.readAsDataURL(file);

    reader.onload = () => {
      const imagenBase64 = reader.result!;
      const base64String = reader.result!.toString().split(',')[1];
      // console.log(reader.result!);
      // console.log(base64String);
      this.imagenBase64 = reader.result!.toString();
    };

  }

}
