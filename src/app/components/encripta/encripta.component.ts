import { Component, OnInit } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-encripta',
  templateUrl: './encripta.component.html',
  styleUrls: ['./encripta.component.css'],
})
export class EncriptaComponent implements OnInit {
  message: string = 'Hola, mundo!';
  //key: any = CryptoJS.enc.Hex.parse('0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef');
  key: any = 'E7FF12D4-AA3A-4E98-844C-0CFC772416E3';
  encryptedMessage: any;
  decryptedMessage: any;

  constructor() {}

  ngOnInit(): void {
    // output: U2FsdGVkX1+Q2MtxA8y0x0KpyuEKoswzBy7Vg0LEb9M=
  }

  onclick() {
    console.log('aqui');
    console.log(this.message);
    console.log(this.key);

    this.encryptedMessage = CryptoJS.AES.encrypt(
      this.message,
      this.key
    ).toString();

    console.log('crypt', this.encryptedMessage);

    // const encryptedMessage2 = 'U2FsdGVkX1+Q2MtxA8y0x0KpyuEKoswzBy7Vg0LEb9M=';
    // const key2 = 'E7FF12D4-AA3A-4E98-844C-0CFC772416E3';

    this.decryptedMessage = CryptoJS.AES.decrypt(
      this.encryptedMessage,
      this.key
    ).toString(CryptoJS.enc.Utf8);

    console.log('decript', this.decryptedMessage); // output: Hola, mundo!
  }
}
