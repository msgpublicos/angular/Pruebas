import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-excel',
  templateUrl: './excel.component.html',
  styleUrls: ['./excel.component.css'],
})
export class ExcelComponent implements OnInit {
  jsonExcel!: any;
  constructor() {}

  ngOnInit(): void {}

  readXlsx(event: any) {
    console.log('event', event);

    let file = event.target.files[0];

    let filereader = new FileReader();
    filereader.readAsBinaryString(file);
    filereader.onload = (e) => {
      var workbook = XLSX.read(filereader.result, { type: 'binary' });
      var sheetname = workbook.SheetNames;
      this.jsonExcel = XLSX.utils.sheet_to_json(workbook.Sheets[sheetname[0]]);
      console.log(this.jsonExcel);
    };
  }
}
