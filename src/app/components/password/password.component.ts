import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.css'],
})
export class PasswordComponent implements OnInit {
  title = 'pruebas';
  passwordForm!: FormGroup;
  passwordIsValid = false;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.passwordForm = this.fb.group({
      password: ['', Validators.required],
    });
  }

  passwordValid(event: boolean) {
    console.log('event: ', event);
    this.passwordIsValid = event;
  }
}
